package pucmg.tcc.sigo.model;

public class NormaVO {

    private String id;
    private String codigo;
    private Integer ano;
    private String titulo;
    private String descricao;

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Integer getAno() {
        return ano;
    }

    public void setAno(Integer ano) {
        this.ano = ano;
    }

    public String getTitulo() { return titulo; }

    public void setTitulo(String titulo) { this.titulo = titulo; }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
