package pucmg.tcc.sigo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RepositorioNormasApplication {

    public static void main(String[] args) {
        SpringApplication.run(RepositorioNormasApplication.class, args);
    }

}
