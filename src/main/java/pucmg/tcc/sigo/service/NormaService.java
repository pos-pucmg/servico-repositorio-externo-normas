package pucmg.tcc.sigo.service;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import pucmg.tcc.sigo.model.NormaVO;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Service
public class NormaService {

    public List<NormaVO> getResumoNormas(String codigo) {
        // IDE
//        URL dirURL = Thread.currentThread().getContextClassLoader().getResource("files");
//        if(dirURL == null) return new ArrayList<>();
//        File dir = new File(dirURL.getPath());

        // Docker
        File dir = new File("/repositorio");
        File[] files;
        if(StringUtils.isEmpty(codigo)) {
            files = dir.listFiles((dir1, name) -> name.endsWith(".pdf"));
        }
        else {
            files = dir.listFiles((dir1, name) -> name.contains(codigo));
        }

        if(files == null)  return new ArrayList<>();
        //System.out.println("achou " + files.length);

        List<NormaVO> listaNormas = new ArrayList<>();
        for(File file : files) {
            NormaVO normaVO = new NormaVO();
            String fileName = StringUtils.stripFilenameExtension(file.getName());
            //System.out.println(fileName);
            normaVO.setId(fileName);
            normaVO.setAno(Integer.valueOf(fileName.split("_")[2]));
            normaVO.setCodigo(fileName.split("_")[0]+" "+fileName.split("_")[1]);

            if(file.getName().contains("NBR 15778")) {
                normaVO.setTitulo("Uniforme escolar - Requisitos de desempenho e segurança");
                normaVO.setDescricao("Esta Norma estabelece os requisitos de desempenho e " +
                        "segurança para uniformes escolares.");
            }
            else if(file.getName().contains("105-B01")) {
                normaVO.setTitulo("Têxteis - Ensaio de solidez da cor. Parte B01: " +
                        "Solidez da cor à luz: Luz do dia");
                normaVO.setDescricao("Descreve um método para determinação da resistência da cor " +
                        "de têxteis de todos os tipos e em todas as formas sob a ação da luz do dia. " +
                        "Permite o uso de dois diferentes conjuntos de tecidos de lã azul de referência. " +
                        "0s resultados dos dois diferentes conjuntos de tecidos de lã azul de referência " +
                        "podem não ser idênticos.");
            }
            else if(file.getName().contains("105-B03")) {
                normaVO.setTitulo("Têxteis - Ensaio de solidez da cor. Parte B03: " +
                        "Solidez da cor ao intemperismo: Exposição ao meio ambiente exterior");
                normaVO.setDescricao("Esta parte da ABNT NBR ISO 105 descreve um método para determinação " +
                        "da resistência da cor de têxteis de todo o tipo, exceto fibras soltas à ação do " +
                        "intemperismo como determinado na exposição ao meio ambiente externo.");
            }
            else if(file.getName().contains("105-B04")) {
                normaVO.setTitulo("Têxteis - Ensaios de solidez da cor. Parte B04: " +
                        "Solidez da cor ao intemperismo artificial: " +
                        "Ensaio de lâmpada de desbotamento ao arco de xenônio");
                normaVO.setDescricao("Esta parte da ABNT NBR ISO 105 descreve um método para determinação " +
                        "da resistência da cor de têxteis de todo o tipo, exceto sobras soltas, às ações " +
                        "das intempéries como determinado pela exposição às condições simuladas de " +
                        "intemperismo num ambiente equipado com uma lâmpada de arco de xenônio.");
            }
            else { continue; }

            listaNormas.add(normaVO);
        }

        return listaNormas;
    }

}
