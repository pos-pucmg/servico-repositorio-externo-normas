package pucmg.tcc.sigo.controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import pucmg.tcc.sigo.model.NormaVO;
import pucmg.tcc.sigo.service.NormaService;

@CrossOrigin
@RestController
@RequestMapping(value="/repositorio")
public class RepositorioNormasController {
//    @Autowired
//    ServletContext context;

    @Autowired
    NormaService normaService;

    @RequestMapping(value = "/download/norma/{fileName}", method = RequestMethod.GET, produces = "application/pdf")
    public ResponseEntity<InputStreamResource> download(@PathVariable("fileName") String fileName) throws IOException {
        //System.out.println("Calling Download:- " + fileName);
        ClassPathResource pdfFile = new ClassPathResource("files/" + fileName+".pdf");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        headers.add("Access-Control-Allow-Origin", "*");
        headers.add("Access-Control-Allow-Methods", "GET, POST, PUT");
        headers.add("Access-Control-Allow-Headers", "Content-Type");
        headers.add("Content-Disposition", "filename=" + fileName);
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
//        headers.add("versao", "1.0");
//        headers.add("descricao", "Legislação acerca do descarte de produtos químicos utilizados na indústria têxtil");

        headers.setContentLength(pdfFile.contentLength());
        return new ResponseEntity<>(
                new InputStreamResource(pdfFile.getInputStream()), headers, HttpStatus.OK);

    }

    @GetMapping(value="/normas", produces = "application/json; charset=UTF-8")
    @ResponseBody
    public ResponseEntity<List<NormaVO>> listar(@RequestParam(value = "codigo",required = false) String codigo) {
        List<NormaVO> listaInfoNormas = normaService.getResumoNormas(codigo);
        if(CollectionUtils.isEmpty(listaInfoNormas)) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(listaInfoNormas, HttpStatus.OK);
    }

}
